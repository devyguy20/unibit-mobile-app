import React from 'react'
import { View, Text, ActivityIndicator, TextInput, Button } from "react-native";
import { applyFilter } from './formFilter';
// Defo will change the hook to a better hook



export default function RegisterScreen(){
    const [email, setEmail] = React.useState('')
    const [password, setPassword] = React.useState('')
    const [applyPassword, setApplyPassword] = React.useState('')
    const [username, setUsername] = React.useState('')
    const [passphrase, setPassphrase] = React.useState('')
    return (
        <View>
            <Text>Username</Text>
            <TextInput placeholder='Username here..' onChangeText={text => setUsername(text)} />
            <Text>Email</Text>
            <TextInput placeholder="Email here.." onChangeText={text => setEmail(text)} />
            <Text>Password</Text>
            <TextInput placeholder="Password here.." onChangeText={text => setPassword(text)}/>
            <Text>Apply Password</Text>
            <TextInput placeholder="Password here.." onChangeText={text => setApplyPassword(text)}/>
            <Text>Wallet passphrase</Text>
            <TextInput placeholder='Passphrase here..' onChangeText={text => setPassphrase(text)} />
            <Button title="Login" onPress={() => applyFilter(email,password,username,passphrase)} />
        </View>
    );
}
