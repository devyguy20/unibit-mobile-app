import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  title: {
    paddingTop: 4,
    fontSize: 18,
    fontWeight: "600",
    paddingBottom: 4,
    letterSpacing: 0.5,
    alignSelf: "center",
    color: "rgba(52,55,65,1)",
    fontFamily: 'LexendSemiBold',
  },
  button: {
    width: 36,
    height: 36,
    paddingTop: 10,
    alignItems: 'center',
  },
});

export default styles;
