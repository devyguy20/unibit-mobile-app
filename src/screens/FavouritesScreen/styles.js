import { StyleSheet } from "react-native";
const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 10,
    backgroundColor: "#FFFF",
  },
  textTitle: {
    top: 15,
    left: 15,
    fontSize: 13,
    color: "rgba(48,50,75,0.5)",
    fontFamily: 'LexendMedium',
  },
});
export default styles;
