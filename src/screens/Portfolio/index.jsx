import React, { Suspense } from "react";
import { View, Text, ActivityIndicator, Dimensions } from "react-native";
import PageHeader from "../../components/PageHeader";
import PortfolioList from "../../components/PortfolioList";

export default function PortfolioScreen() {
  return (
    <View style={{ flex: 1, justifyContent: "center" }}>
      <PageHeader title={"Portfolio"}/>
        <PortfolioList />
    </View>
  );
}
