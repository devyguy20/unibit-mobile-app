import { initializeApp } from 'firebase/app'
import { Firestore, collection, getDocs, getFirestore, where } from 'firebase/firestore'
import { doc, setDoc, getDoc, query } from 'firebase/firestore'
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword, updateProfile } from 'firebase/auth'
import axios from 'axios'
import { coins } from '../services/bitgo'
export interface userModel {
    username: string,
    email: string,
    password: string,
    bitgo_wallet_ids?: Object
}


// Should only allow client users to only access their own information
const firebaseConfig = {

}
const app = initializeApp(firebaseConfig);
// Incase the idea was to keep firebase access out of user's hands, You can fully ignore the part above
// And just disable the boolean below
const ACCESS: string = "DIRECT"
const GATEWAY_IP = "" // Address of the backend server that provides firebase services

// Register, for now... half assed addition because doesn't detect existing users 'yet'
export const registerFirebaseUser = async (user: userModel) => {
    const auth = getAuth(app)
    const userAuth = await createUserWithEmailAndPassword(auth, user.email, user.password)
    const data = {at: userAuth.user.getIdToken(true)}
    updateProfile(userAuth.user,{displayName: user.username})
    for (let coin in coins)
    {
        data["data"][coin+"_wallet"] =  user.bitgo_wallet_ids[coin]
    }
    axios.post(GATEWAY_IP+"/user",data)
}
export const loginFirebaseUser = async (email: string, password: string) => {
    let data;
    const auth = getAuth(app)
    const userAuth = await signInWithEmailAndPassword(auth,email,password)
    return userAuth
}
