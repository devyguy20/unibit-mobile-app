import {registerFirebaseUser, loginFirebaseUser} from '../../services/firebase'
const MIN_PASSWORD = 5
const MAX_PASSWORD = 50


export const applyFilter = (email: string,password: string) => {
    const emailRegex = new RegExp("^(.+)@(.+)\.(.+)$");
    // Check if email has even the correct correlation to email
    if (emailRegex.test(email))
    {
        console.error("FAIL")
        return
    }
    if (password.length > MAX_PASSWORD || password.length < MIN_PASSWORD)
    {
        console.error("ERROR")
        return
    }
    const res = loginFirebaseUser(email,password) //?
}
