import { StatusBar } from "expo-status-bar";
import { StyleSheet, View, ActivityIndicator } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import Navigation from "./src/navigation";
import FavouritesProvider from "./src/context/FavouritesProvider";
import { RecoilRoot } from "recoil";
import { useFonts, Inter_900Black } from "@expo-google-fonts/inter";

export default function App() {
  let [fontsLoaded] = useFonts({
    Inter_900Black,
    LexendMedium: require("./assets/fonts/Lexend-Medium.ttf"),
    LexendRegular: require("./assets/fonts/Lexend-Regular.ttf"),
    LexendSemiBold: require("./assets/fonts/Lexend-SemiBold.ttf"),
    RobotoRegular: require("./assets/fonts/Roboto-Regular.ttf"),
    RobotoMedium: require("./assets/fonts/Roboto-Medium.ttf"),
  });

  if (!fontsLoaded) {
    return <ActivityIndicator size={"large"} />;
  }
  return (
    <NavigationContainer
      theme={{
        colors: {
          background: "#FFFF",
        },
      }}
    >
      <RecoilRoot>
        <FavouritesProvider>
          <View style={styles.container}>
            <Navigation />
            <StatusBar style="dark" />
          </View>
        </FavouritesProvider>
      </RecoilRoot>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFF",
    paddingTop: 50,
  },
});
