import React, { useState, useEffect } from "react";
import { FlatList, Text, View, Dimensions } from "react-native";
import { useFavourites } from "./../../context/FavouritesProvider";
import FavCoinCard from "./../../components/FavCoinCard";
import { getFavouritesCoins } from "./../../services/requests";
import styles from "./styles";
const categories = [
  {
    title: 'Price',
  },
  {
    title: '24H Change',
  },
  {
    title: '24H High',
  },
  {
    title: '24H Low',
  },
  {
    title: 'Market Cap',
  },
];

export default function Favourites() {
  const screenWidth = Dimensions.get("window").Width;
  const { favouritesCoinIds } = useFavourites();
  const [coins, setCoins] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const convertCoinIds = () => favouritesCoinIds.join("%2C");

  const fetchFavouritesCoins = async () => {
    if (isLoading) {
      return;
    }
    setIsLoading(true);
    const favouritesCoinsData = await getFavouritesCoins(1, convertCoinIds());
    setCoins(favouritesCoinsData);
    setIsLoading(false);
  };
  useEffect(() => {
    if (favouritesCoinIds.length > 0) {
      fetchFavouritesCoins();
    }
  }, [favouritesCoinIds]);

  return (


    (favouritesCoinIds.length > 0 ? <View style={{ backgroundColor: '#FFFF' }}>
      <FlatList
        ListHeaderComponent={({ item }) => {
          return (<View>
          </View>)
        }}
        bounces={false}
        data={coins}
        ItemSeparatorComponent={<View style={{ height:1, backgroundColor:"rgba(240,240,245,0.75)", marginHorizontal:15 }} />}
        renderItem={({ item }) => <FavCoinCard marketCoin={item} />}
        keyExtractor={(contact, index) => String(index)}
        style={{ paddingVertical:15 }}
      />
    </View> : <View />)

  );
}
