import { View, Text, Pressable, Dimensions, FlatList } from "react-native";
import React, { useState, useEffect } from "react";
import AwesomeButton from "react-native-really-awesome-button";
import { useNavigation } from "@react-navigation/native";
import { useRecoilValue, useRecoilState } from "recoil";
import { SwipeListView } from "react-native-swipe-list-view";
import { VictoryLabel, VictoryPie } from "victory-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import styles from "./styles";
import PortfolioItem from "../PortfolioItem";
import {
  allPortfolioAssets,
  allPortfolioBoughtAssetsInStorage,
} from "./../../atoms/PortfolioAssets";

export default function PortfolioList() {


  const categories = [
    {
      title: 'Asset',
    },
    {
      title: 'Amount',
    },
    {
      title: 'Value',
    },
  ];

  const navigation = useNavigation();
  const screenWidth = Dimensions.get("window").Width;
  // const [assets, setAssets] = useRecoilState(allPortfolioAssets);
  const assets = useRecoilValue(allPortfolioAssets);
  const [storageAssets, setStorageAssets] = useRecoilState(
    allPortfolioBoughtAssetsInStorage
  );
  const getCurrentBalance = () =>
    assets.reduce(
      (total, currentAsset) =>
        total + currentAsset.currentPrice * currentAsset.quantityBought,
      0
    );

  const getCurrentValueChange = () => {
    const currentBalance = getCurrentBalance();
    const boughtBalance = assets.reduce(
      (total, currentAsset) =>
        total + currentAsset.priceBought * currentAsset.quantityBought,
      0
    );
    return (currentBalance - boughtBalance).toFixed(2);
  };

  const getCurrentPercentageChange = () => {
    const currentBalance = getCurrentBalance();
    const boughtBalance = assets.reduce(
      (total, currentAsset) =>
        total + currentAsset.priceBought * currentAsset.quantityBought,
      0
    );
    return (
      (((currentBalance - boughtBalance) / boughtBalance) * 100).toFixed(2) || 0
    );
  };
  const formatCurrentBalance = (fn) => {
    const totalFixed = new Intl.NumberFormat().format(fn);
    return totalFixed;
  };
  const handleDeleteAsset = async (asset) => {
    const newAssets = storageAssets.filter(
      (coin) => coin.unique_id !== asset.item.unique_id
    );
    const jsonValue = JSON.stringify(newAssets);
    await AsyncStorage.setItem("@portfolio_coins", jsonValue);
    setStorageAssets(newAssets);
  };
  const renderDeleteButton = (data) => {
    return (
      <Pressable
        style={styles.deleteButtonContainer}
        onPress={() => handleDeleteAsset(data)}
      >
        <Text>Buy More</Text>
      </Pressable>
    );
  };





  const wantedGraphicData = [
    { symbol: 'BTC', balance: 21500000 },
    { symbol: 'ETH', balance: 8538000 },
    { symbol: 'DOT', balance: 7132000 },
    { symbol: 'DOGE', balance: 2990010 },


  ]

  const graphicColor = ['rgba(0,140,182,0.98)', 'rgba(0,140,182,0.80)', 'rgba(0,140,182,0.65)', 'rgba(0,140,182,0.5)', 'rgba(0,140,182,0.35)', 'rgba(0,140,182,0.25)', 'rgba(0,140,182,0.15)', 'rgba(0,140,182,0.10)']; // Colors
  const defaultGraphicData = [{ balance: 0 }, { balance: 0 }, { balance: 100 }]; // Data used to make the animate prop work
  
    const [graphicData, setGraphicData] = useState(defaultGraphicData);
  
    useEffect(() => {
      setGraphicData(wantedGraphicData); // Setting the data that we want to display
    }, []);



  return (
    <SwipeListView
      data={assets}
      renderItem={({ item }) => <PortfolioItem item={item} />}




      //fix warning if repeat same coin and prevent if delete don't delete all same coins.
      keyExtractor={({ id }, index) => `${id}${index}`}
      ListHeaderComponent={
        <>
          <View style={styles.balanceContainer}>
            <View style={{ alignSelf: 'center', marginTop:5 }}>
              <Text style={styles.currentBalance}>Total Balance</Text>
              <Text style={styles.currentBalanceValue}>
                ${formatCurrentBalance(getCurrentBalance().toFixed(2))}
              </Text>
              {getCurrentValueChange >= 0 ?
                  <Text
                    style={{
                      ...styles.valueChange,
                      color: "#008F87",
                    }}
                  >{" "}+${formatCurrentBalance(getCurrentValueChange()) * -1} (Total profit)
                  </Text>
                  : <Text
                    style={{
                      ...styles.valueChange,
                      color: "#BC1B72",
                    }}
                  >{" "}-${formatCurrentBalance(getCurrentValueChange()) * -1} (Total profit)
                  </Text>}
            </View>
          </View>
          <View style={{ marginTop:15, top:2.5, alignSelf:'center' }}>
          <VictoryPie
        labelComponent={<VictoryLabel style={{ fontFamily:'Helvetica Neue', fontWeight:'500', fontSize: 13 }} />}
        textComponent={<Text style={{ color:'green' }} />}
        animate={{ easing: 'exp' }}
        data={graphicData}
        width={380}
        height={320}
        x={"symbol"}
        y={"balance"}
        colorScale={graphicColor}
        innerRadius={80}
      />
          </View>

          <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'space-evenly', paddingHorizontal: 15, borderBottomWidth:2, borderBottomColor:'rgba(230,230,233,0.5)' }}>
            <AwesomeButton
              height={48}
              style={styles.button}
              borderColor={"rgba(0,140,182,1)"}
              backgroundColor={"#FFFF"}
              borderRadius={8}
              borderWidth={1.5}
              raiseLevel={0}
              width={110}
              type="primary">
              <Text style={styles.buttonText}>Buy/Sell</Text>
            </AwesomeButton>
            <AwesomeButton
              height={48}
              style={styles.button}
              borderColor={"rgba(0,140,182,1)"}
              backgroundColor={"#FFFF"}
              borderRadius={8}
              borderWidth={1.5}
              raiseLevel={0}
              width={110}
              type="primary">
              <Text style={styles.buttonText}>Deposit</Text>
            </AwesomeButton>
            <AwesomeButton
              height={48}
              style={styles.button}
              borderColor={"rgba(0,140,182,1)"}
              backgroundColor={"#FFFF"}
              borderRadius={8}
              borderWidth={1.5}
              raiseLevel={0}
              width={110}
              type="primary">
              <Text style={styles.buttonText}>Withdraw</Text>
            </AwesomeButton>
          </View>

          <FlatList
            horizontal
            data={categories}
            scrollEnabled={false}
            showsHorizontalScrollIndicator={false}
            renderItem={({ item }) => <View style={{ height: 38, alignSelf: 'flex-end', width: 154 }}><Text style={styles.textTitle}>{item.title}</Text></View>}
            keyExtractor={item => item.title}

          />

        </>
      }
      renderHiddenItem={(data) => renderDeleteButton(data)}
      rightOpenValue={-160}
      ListFooterComponent={
        <Pressable
          style={styles.buttonContainer}
          onPress={() => navigation.navigate("AddNewAssetScreen")}
        >
          <Text style={styles.buttonText}>Add Custom Asset</Text>
        </Pressable>
      }
      disableRightSwipe
      closeOnRowPress

    />
  );
}
