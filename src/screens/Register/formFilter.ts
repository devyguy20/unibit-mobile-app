import {registerFirebaseUser} from '../../services/firebase'
import { createHotWallets } from '../../services/bitgo'
import AsyncStorage from "@react-native-async-storage/async-storage";
const MIN_PASSWORD = 5
const MAX_PASSWORD = 50


export const applyFilter = async (email: string,password: string, username: string, passphrase: string) => {
    const emailRegex = new RegExp("^(.+)@(.+)\.(.+)$");
    // Check if email has even the correct correlation to email
    if (emailRegex.test(email))
    {
        console.error("FAIL")
        return
    }
    if (password.length > MAX_PASSWORD || password.length < MIN_PASSWORD)
    {
        console.error("ERROR")
        return
    }
    const walletRes = await createHotWallets(passphrase)
    const res = registerFirebaseUser({
        username: username,
        email: email,
        password: password,
        bitgo_wallet_ids: walletRes
    }) //?
}

export const confirmWallet = async (walletPassphrase: string) => {
    // If something happens
    const res = await createHotWallets(walletPassphrase)

}