import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  balanceContainer: {
    flexDirection: "row",
    alignSelf: 'center',
    alignItems: "center",
    marginTop: 4,
    marginBottom: 5,
  },
  currentBalance: {
    marginTop: 2,
    marginBottom: 6,
    fontSize: 13,
    alignSelf: 'center',
    color: "rgba(48,50,75,0.5)",
    fontFamily: 'LexendMedium',
  },
  portfolioTextCell: {
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'center'
  },
  currentBalanceValue: {
    color: "rgba(52,55,65,1)",
    fontSize: 30,
    fontFamily: 'RobotoMedium',
    alignSelf: 'center',
    fontWeight: "500",
    letterSpacing: 1,
  },
  valueChange: {
    top: 5,
    fontSize: 13.5,
    fontFamily: 'RobotoMedium',
    letterSpacing: 0.5,
  },
  textTitle: {
    top: 15,
    left: 15,
    fontSize: 13,
    color: "rgba(48,50,75,0.4)",
    fontFamily: 'LexendMedium',
  },
  priceChangePercentageContainer: {
    flexDirection: "row",
    paddingVertical: 8,
    paddingHorizontal: 5,
    borderRadius: 5,
  },
  assetsTitle: {
    color: "white",
    fontWeight: "700",
    fontSize: 23,
    paddingVertical: 20,
    paddingHorizontal: 10,
  },
  button: {
    bottom:6,
    marginTop: 18,
    marginVertical: 10,
    marginHorizontal:10,
  },
  buttonText: {
    fontSize: 15.5,
    color: "rgba(0,140,182,1)",
    marginBottom: 2,
    fontFamily: "LexendRegular",
  },
  deleteButtonContainer: {
    flex: 1,
    alignItems: "flex-end",
    justifyContent: "center",
    paddingHorizontal:40
  },
});

export default styles;
