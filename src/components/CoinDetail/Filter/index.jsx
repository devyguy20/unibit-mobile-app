import { Text, Pressable, Dimensions } from "react-native";
import React, { memo } from "react";

const Filter = (props) => {
  const { filterDay, filterText, selectedRange, setSelectedRange } = props;
  const screenWidth = Dimensions.get("window").width;

  const isSelected = (filter) => filter === selectedRange;
  return (
    <Pressable
      style={{
        width: screenWidth/5,
        paddingBottom: 8,
        alignItems: 'center',
        borderBottomWidth: 2,
        borderBottomColor: isSelected(filterDay) ? "rgba(0,140,182,1)" : "rgba(230,230,233,0.5)",
      }}
      onPress={() => setSelectedRange(filterDay)}
    >
      <Text style={{ color: isSelected(filterDay) ? "rgba(0,140,182,1)" : "rgba(48,50,75,0.6)",fontSize: 13, bottom:2, fontFamily: 'LexendMedium' }}>
        {filterText}
      </Text>
    </Pressable>
  );
};
export default memo(Filter);
