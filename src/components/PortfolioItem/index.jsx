import { View, Text, Image } from "react-native";
import React from "react";
import styles from "./styles";

export default function PortfolioItem({ item }) {
  const {
    currentPrice,
    name,
    priceChangePercentage,
    quantityBought,
    ticker,
  } = item;

  const isChangePositive = () => {
    priceChangePercentage >= 0;
  };

  const renderHoldings = () => (quantityBought * currentPrice).toFixed(2);

  const formatText = (name) => {
    if (name.length >= 11) {
      const result = name.slice(0, 11);
      const fixedName = result + "...";
      return fixedName;
    }
    return name;
  };

  return (
    <View style={styles.itemContainer}>
      <View>
        <Text style={styles.coinName}>{ticker}</Text>
        <Text style={styles.coinCode}>{formatText(name)}</Text>
      </View>
      <View style={styles.quantityContainer}>
        <Text style={styles.text}>{quantityBought}</Text>
      </View>
      <View style={styles.priceContainer}>
        <Text style={styles.text}>${renderHoldings()}</Text>
      </View>
    </View>
  );
}
