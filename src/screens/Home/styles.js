import { StyleSheet } from "react-native";
const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 10,
    backgroundColor: "#FFFF",
  },
  title: {
    paddingTop: 4,
    fontSize: 18,
    fontWeight: "600",
    paddingBottom: 4,
    letterSpacing: 0.5,
    alignSelf: "center",
    color: "rgba(52,55,65,1)",
    fontFamily: 'LexendSemiBold',
  },
  button: {
    width: 36,
    height: 36,
    paddingTop: 10,
    alignItems: 'center',
  },
  text: {
    top: 8,
    fontSize: 13,
    alignSelf: 'center',
    color: "rgba(48,50,75,0.75)",
    fontFamily: 'LexendRegular',
  },
  percentage: {
    top: 4,
    fontSize: 12.25,
    color: "#FFFF",
    alignSelf: "center",
    fontFamily: 'RobotoMedium',
  },
  textTitle: {
    top: 15,
    left: 15,
    fontSize: 13,
    color: "rgba(48,50,75,0.4)",
    fontFamily: 'LexendMedium',
  },
  filterContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    borderRadius: 10,
    paddingVertical: 3,
    // borderRadius: 5,
    // marginHorizontal: 5,
    marginTop: 10,
  },
});
export default styles;
