import { View, Text, ActivityIndicator, TextInput, Button } from "react-native";
import { applyFilter } from './formFilter'
import React from 'react'


export default function LoginScreen(){
    const [email, setEmail] = React.useState('')
    const [password, setPassword] = React.useState('')
    return (
        <View>
            <Text>Email</Text>
            <TextInput placeholder="Email here.." onChangeText={text => setEmail(text)} />
            <Text>Password</Text>
            <TextInput placeholder="Password here.." onChangeText={text => setPassword(text)}/>
            <Button title="Login" onPress={() => applyFilter(email,password)} />
        </View>
    );
}