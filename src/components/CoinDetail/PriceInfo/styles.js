import { StyleSheet } from "react-native";
const styles = StyleSheet.create({
  coinName: {
    color: "rgba(52,55,65,1)",
    fontFamily: 'LexendMedium',
    textTransform: 'uppercase',
    fontSize: 15,
  },
  priceTitle: {
    marginTop: 2,
    fontSize: 12.5,
    marginBottom: 6,
    color: "rgba(48,50,75,0.5)",
    fontFamily: 'RobotoMedium',
  },
  priceText: {
    color: "rgba(52,55,65,1)",
    fontSize: 30,
    fontFamily: 'RobotoMedium',
    fontWeight: "500",
    letterSpacing: 1,
  },
  priceContainer: {
    padding: 15,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  percentageContainer: {
    padding: 5,
    borderRadius: 5,
    flexDirection: "row",
  },
  percentage: {
    fontSize: 12.25,
    color: "#FFFF",
    fontFamily: 'RobotoMedium',
  },
});
export default styles;
