import { StyleSheet, Dimensions } from "react-native";

const screenWidth = Dimensions.get("window").width;
const styles = StyleSheet.create({
  headerContainer: {
    left: 4,
    paddingVertical: 10,
    paddingHorizontal: 15,
    width: screenWidth,
    alignSelf: 'center',
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  button: {
    width: 30,
    height: 30,
  },
  coinContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  nameText: {
    color: "rgba(52,55,65,1)",
    fontFamily: "LexendMedium",
    marginHorizontal: 5,
    fontSize: 16,
  },
  symbolText: {
    color: "rgba(48,50,75,0.65)",
    fontFamily: "LexendRegular",
    marginHorizontal: 5,
    fontSize: 15,
  },
});
export default styles;
