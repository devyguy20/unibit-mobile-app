import React from "react";
import { Text, View, Dimensions, Pressable } from "react-native";
import styles from "./styles";
import * as shape from 'd3-shape';
import { FontAwesome } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";
import { LineChart, LineChartProvider } from "react-native-wagmi-charts";



export default function index({ marketCoin }) {
  const {
    id,
    name,
    symbol,
    current_price
  } = marketCoin;
  const navigation = useNavigation();
  const screenWidth = Dimensions.get("window").width;
  const chartChangedColor =
    1 > 0 ? "#008F87" : "#BC1B72";
    

  const data = [
    {
      timestamp: 1625945400000,
      value: 33575.25,
    },
    {
      timestamp: 1625945400000,
      value: 33575.25,
    },
    {
      timestamp: 12128932,
      value: 33475.25,
    },
    {
      timestamp: 1625946300000,
      value: 33545.25,
    },
    {
      timestamp: 1625947200000,
      value: 33510.25,
    },
    {
      timestamp: 1625948100000,
      value: 33215.25,
    },
  ];

  return (
    <Pressable
      style={styles.cardContainer}
      onPress={() => navigation.navigate("CoinDetail", { coinId: id })}
    >
      <FontAwesome
        style={{ top: 30, position: 'absolute' }}
        name={'star'}
        size={10}
        color={"rgba(52,55,65,0.3)"}
      />
      <View style={{ left: 15 }}>
        <Text numberOfLines={1} style={styles.title}>{symbol}</Text>
        <View style={styles.dataContainer}>
          <Text numberOfLines={1} style={styles.ticket}>{name}</Text>
        </View>
      </View>


      <LineChart.Provider data={data}>
        <LineChart shape={shape.curveLinear} height={58} width={screenWidth / 4} style={{ alignSelf: 'center', position: 'absolute', justifyContent: 'center', left: screenWidth - screenWidth / 1.5, opacity:0.95 }}>
          <LineChart.Path pathProps={{ strokeLinecap: 'round', strokeLinejoin: 'round' }} color={chartChangedColor} width={1.25}>
            <LineChart.Gradient color={chartChangedColor} opacity={0.4} />
          </LineChart.Path>
        </LineChart>
      </LineChart.Provider>


      <View style={styles.itemColumn}>
        <Text style={styles.price}>${current_price.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 })}</Text>
      </View>
    </Pressable>
  );
}
