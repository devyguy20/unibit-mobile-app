import React from "react";
import { Text, View, Image, Pressable } from "react-native";
import styles from "./styles";
import { FontAwesome } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";


export default function index({ marketCoin }) {
  const {
    id,
    name,
    image,
    symbol,
    isfav,
  } = marketCoin;
  const navigation = useNavigation();

  return (
    <Pressable
      style={styles.cardContainer}
      onPress={() => navigation.navigate("CoinDetail", { coinId: id })}
    >
      <View>
        <Text numberOfLines={1} style={styles.title}>{symbol}</Text>
        <View style={styles.dataContainer}>
          <Text numberOfLines={1} style={styles.ticket}>{name}</Text>
        </View>
      </View>
    </Pressable>
  );
}
