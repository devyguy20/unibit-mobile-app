import { StyleSheet, Dimensions } from "react-native";
const screenWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
  cardContainer: {
    flexDirection: "row",
    height: 70,
    justifyContent: "space-between",
    width: screenWidth - 30,
    marginHorizontal:15,
    paddingVertical: 15,
  },
  image: {
    opacity: 0.9,
    width: 21,
    height: 21,
  },
  title: {
    color: "rgba(52,55,65,1)",
    fontSize: 15,
    textTransform: "uppercase",
    letterSpacing: 0.25,
    width: 100,
    fontFamily: 'LexendMedium',
    fontWeight: "500",
  },
  dataContainer: {
    flexDirection: "row",
    marginTop: 4,
  },
  ticket: {
    width: 62,
    fontSize: 11.5,
    color: "rgba(48,50,75,0.6)",
    marginRight: 5,
    fontFamily: 'RobotoMedium',
  },
  price: {
    top: 1,
    fontSize: 15,
    fontFamily: 'RobotoMedium',
    alignSelf: 'flex-start',
    textAlign: 'left',
    fontWeight: "600",
    color: "rgba(52,55,65,1)",
  },
  itemColumn: {
    alignSelf:'center'
  }
});
export default styles;
