import React from "react";
import { Text, View, Image, Pressable } from "react-native";
import styles from "./styles";
import { useNavigation } from "@react-navigation/native";

export default function index({ marketCoin }) {
  const {
    id,
    market_cap,
    current_price,
    high_24h,
    low_24h,
    price_change_percentage_24h,
  } = marketCoin;
  const navigation = useNavigation();
  const normalizeMarketCap = (marketCap) => {
    if (marketCap > 1e12) {
      return `${(marketCap / 1e12).toFixed(1)}T`;
    }
    if (marketCap > 1e9) {
      return `${(marketCap / 1e9).toFixed(1)}B`;
    }
    if (marketCap > 1e6) {
      return `${(marketCap / 1e6).toFixed(1)}M`;
    }
    if (marketCap > 1e3) {
      return `${(marketCap / 1e3).toFixed(1)}K`;
    }
    return marketCap;
  };

  const percentageColor =
    price_change_percentage_24h < 0 ? "#BC1B72" : "#008F87" || "white";
  return (
    <Pressable
      style={styles.cardContainer}
      onPress={() => navigation.navigate("CoinDetail", { coinId: id })}>
      <View style={styles.itemColumn}>
        <Text style={styles.price}>${current_price.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2})}</Text>
      </View>
      <View style={styles.itemColumn}>
      <Text style={[styles.price, { color: percentageColor, opacity:0.85 }]}>
{price_change_percentage_24h < 0 ? "↓" : "↑"}{price_change_percentage_24h.toFixed(2).replace(/^-/, "")}%
        </Text>
      </View>
      <View style={styles.itemColumn}>
        <Text style={styles.price}>${high_24h.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2})}</Text>
      </View>
      <View style={styles.itemColumn}>
        <Text style={styles.price}>${low_24h.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2})}</Text>
      </View>
      <View style={styles.itemColumn}>
        <Text style={styles.price}>${normalizeMarketCap(market_cap)}</Text>
      </View>
    </Pressable>
  );
}
