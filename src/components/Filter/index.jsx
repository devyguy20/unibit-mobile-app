import { Text, Pressable, Dimensions } from "react-native";
import React, { memo } from "react";

const Filter = (props) => {
  const { filterValue, filterText, selectedRange, setSelectedRange } = props;
  const screenWidth = Dimensions.get("window").width;

  const isSelected = (filter) => filter === selectedRange;
  return (
    <Pressable
      style={{
        width: screenWidth/4,
        paddingBottom: 8,
        alignItems: 'center',
        borderBottomWidth: 2,
        borderBottomColor: isSelected(filterValue) ? "rgba(0,140,182,1)" : "rgba(230,230,233,0.5)",
      }}
      onPress={() => setSelectedRange(filterValue)}
    >
      <Text style={{ color: isSelected(filterValue) ? "rgba(0,140,182,1)" : "rgba(48,50,75,0.6)",fontSize: 13, bottom:2, fontFamily: 'LexendMedium' }}>
        {filterText}
      </Text>
    </Pressable>
  );
};
export default memo(Filter);
