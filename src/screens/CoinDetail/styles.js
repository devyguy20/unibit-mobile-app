import { StyleSheet } from "react-native";
const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: "#FFFF",
  },
  priceConverterContainer: {
    borderBottomWidth: 1,
    borderBottomColor: "rgba(240,240,245,0.75)",
  },
  textTitle: {
    fontSize: 11.5,
    marginTop: 14,
    marginBottom: 8,
    paddingHorizontal: 2.5,
    color: "rgba(48,50,75,0.6)",
    fontFamily: 'RobotoMedium',
  },
  row: {
    alignSelf:'center',
    flexDirection:'row',
  },
  text: {
    marginTop: 2,
    marginBottom: 14,
    paddingHorizontal: 2.5,
    color: "rgba(52,55,65,1)",
    fontFamily: "RobotoMedium",
    fontSize: 15,
  },
  priceTitle: {
    marginTop: 4,
    marginBottom: 2,
    fontSize: 13,
    color: "rgba(48,50,75,0.5)",
    fontFamily: 'LexendMedium',
  },
  priceText: {
    color: "rgba(52,55,65,1)",
    fontSize: 30,
    fontFamily: 'RobotoMedium',
    fontWeight: "500",
    letterSpacing: 1,
  },
  dateText: {
    left:5,
    fontSize: 12.25,
    marginTop: 2.5,
    fontFamily: 'RobotoMedium',
    color: "rgba(52,55,65,0.4)",
    fontWeight: "500",
    letterSpacing: 1,
  },
  priceContainer: {
    padding: 15,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  percentageContainer: {
    padding: 5,
    borderRadius: 5,
    flexDirection: "row",
  },
  percentage: {
    fontSize: 12.25,
    color: "#FFFF",
    fontFamily: 'RobotoMedium',
  },
  buttonText: {
    fontSize: 16,
    color: "#FFFF",
    marginBottom: 2,
    letterSpacing: 0.15,
    fontFamily: "LexendRegular",
  },
  modalHeader: {
    paddingTop:50
  },
  filterContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    borderRadius: 10,
    paddingVertical: 3,
    // borderRadius: 5,
    // marginHorizontal: 5,
    marginTop: 10,
  },
});
export default styles;
