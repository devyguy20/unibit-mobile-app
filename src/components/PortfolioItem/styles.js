import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  itemContainer: {
    height: 70,
    backgroundColor:'#FFFF',
    flexDirection: "row",
    justifyContent: "space-between",
    borderBottomWidth: 1,
    borderBottomColor: "rgba(240,240,245,0.75)",
    marginHorizontal:15,
    paddingVertical: 15,
  },
  coinName: {
    color: "rgba(52,55,65,1)",
    fontSize: 15,
    textTransform: "uppercase",
    letterSpacing: 0.25,
    width: 100,
    fontFamily: 'LexendMedium',
    fontWeight: "500",
  },
  text: {
    top: 10,
    fontSize: 15,
    fontWeight: "600",
    fontFamily: 'RobotoMedium',
    color: "rgba(52,55,65,1)",
  },
  image: {
    top: 8,
    opacity: 0.9,
    marginRight:6,
    width: 21,
    height: 21,
  },
  quantityContainer: {
    right: 10,
    textAlign: 'left',
    marginLeft: "auto",
    alignItems: "flex-end",
  },
  coinCode: {
    width: 62,
    top: 4,
    fontSize: 11.5,
    color: "rgba(48,50,75,0.6)",
    marginRight: 5,
    fontFamily: 'RobotoMedium',
  },
  priceContainer: {
    marginLeft: "auto",
    alignItems: "flex-end",
  },
});

export default styles;
