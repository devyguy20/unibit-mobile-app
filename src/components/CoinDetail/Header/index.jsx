import React from "react";
import { View, Text, Image, Pressable } from "react-native";
import { Octicons, FontAwesome } from "@expo/vector-icons";
import styles from "./styles";
import { useNavigation } from "@react-navigation/native";
import { useFavourites } from "./../../../context/FavouritesProvider";

export default function index(props) {
  const { coinId, image, symbol, name } = props;
  const navigation = useNavigation();
  const { favouritesCoinIds, storeFavouritesCoinId, removeFavouritesCoinId } =
    useFavourites();

  const checkIfIsFavourite = () =>
    favouritesCoinIds.some((coinIdValue) => coinIdValue === coinId);

  const handleFavouriteCoin = () => {
    if (checkIfIsFavourite()) {
      return removeFavouritesCoinId(coinId);
    }
    return storeFavouritesCoinId(coinId);
  };
  return (
    <View style={styles.headerContainer}>
      <Pressable style={styles.button} onPress={() => navigation.goBack()}>
      <Octicons
        name="chevron-left"
        size={26}
        color="rgba(52,55,65,1)"
      />
      </Pressable>
      <View style={styles.coinContainer}>
        <Text style={styles.nameText}>{name}</Text>
        <Text style={{ fontSize:18, bottom:2, color: "rgba(48,50,75,0.15)" }}>|</Text>
        <Text style={styles.symbolText}>{symbol.toUpperCase()}</Text>
      </View>
      <Pressable style={styles.button} onPress={() => handleFavouriteCoin()}>
      <FontAwesome
        style={{ top:4, opacity:0.9 }}
        name={checkIfIsFavourite() ? "star" : "star-o"}
        size={21}
        color={checkIfIsFavourite() ? "rgba(0,140,182,1)" : "rgba(52,55,65,0.75)"}
      />
      </Pressable>
    </View>
  );
}
