import { StyleSheet } from "react-native";
const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 10,
    backgroundColor: "#FFFF",
  },
  textTitle: {
    top: 15,
    left: 15,
    fontSize: 13,
    color: "rgba(48,50,75,0.5)",
    fontFamily: 'LexendMedium',
  },
  title: {
    paddingTop: 4,
    fontSize: 20,
    fontWeight: "600",
    paddingBottom: 4,
    letterSpacing: 0.5,
    alignSelf: "center",
    color: "rgba(52,55,65,1)",
    fontFamily: 'LexendSemiBold',
  },
});
export default styles;
