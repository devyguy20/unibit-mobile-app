import { View, Text, Pressable } from "react-native";
import { Octicons } from "@expo/vector-icons";
import React from "react";
import styles from "./styles";
import { AntDesign } from "@expo/vector-icons";

export default function PageHeader({ title }) {

  const isChangePositive = () => {
    priceChangePercentage >= 0;
  };

  const renderHoldings = () => (quantityBought * currentPrice).toFixed(2);

  const formatText = (name) => {
    if (name.length >= 11) {
      const result = name.slice(0, 11);
      const fixedName = result + "...";
      return fixedName;
    }
    return name;
  };

  return (
    <View style={{ paddingBottom: 10, flexDirection:'row', justifyContent:'space-between', paddingHorizontal:10 }} zIndex={999999}>
    <Pressable style={styles.button} onPress={() => console.log('DoMore')}>
    <Octicons name="kebab-horizontal" size={20} color="rgba(52,55,65,1)" />
    </Pressable>
      <Text style={styles.title}>{title}</Text>
      <Pressable style={styles.button} onPress={() => console.log('DoSearch')}>
    <Octicons name="search" size={20} color="rgba(52,55,65,1)" />
    </Pressable>
    </View>
  );
}
