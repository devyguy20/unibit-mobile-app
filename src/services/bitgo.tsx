const { BitGo } = require('bitgo');



const SPENDING_LIMIT = "40000"
const WALLET_NAME = "unibit personal wallet"
export const coins = ["gteth","tbtc","tsol"]


// Policy of a coin in a wallet
const factoryPolicy = () => {
  return {
    id: "spendingLimit",
    type: "advancedWhitelist",
    condition: {
      amountString: SPENDING_LIMIT,
    },
    action: {
      type: "deny",
    },
  }
}

// Create transaction parameters
const factoryTransaction = (address: String, amount: String) => {
  return {
    recipients: [{
      amount: amount,
      address: address
    }]
  }
}
// Prebuild the transaction, we don't handle the transaction itself, should send to user email verification video
const commitTransaction = (transactionParams: Object, wallet: any) => wallet.prebuildTransasction(transactionParams)

// Fill in with actual access token, each *user* will have its own id, it identifies them in the bitgo frame
const accessToken = '<your_actual_access_token>';

// Initialize the SDK
const bitgo = new BitGo({
accessToken: accessToken,
env: 'test',
});


// Create a wallet of type coin
export const createHotWallets = async (passphrase: String) => {
  let wallet_ids = {}
  for(let coin in coins)
  {
    let wallet = await bitgo.coin(coin).wallets().generateWallet({
      label: WALLET_NAME,
      passphrase: passphrase
    })
    wallet_ids[coin] = wallet.wallet.id
  }
  return wallet_ids
}
// https://developers.bitgo.com/guides/wallets/create/addresses
const createAddress = async (wallet: any) => await wallet.createAddress()
