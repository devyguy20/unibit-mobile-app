import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  ScrollView,
  Dimensions,
  ActivityIndicator,
} from "react-native";
import { Pressable } from "react-native";
import Modal from "react-native-modal";
import { LineChart, LineChartProvider } from "react-native-wagmi-charts";
import styles from "./styles";
import * as haptics from 'expo-haptics';
import * as shape from 'd3-shape';
import CoinDetailHeader from "./../../components/CoinDetail/Header";
import { useRoute } from "@react-navigation/native";
import {
  getDetailedCoinData,
  getCoinMarketChart,
} from "./../../services/requests";
import Filter from "../../components/CoinDetail/Filter";
import AwesomeButton from "react-native-really-awesome-button";
import { Octicons } from "@expo/vector-icons";
import WebView from "react-native-webview";
import { LinearGradient } from "expo-linear-gradient";

const filterDaysArray = [
  { filterDay: "0.04166", filterText: "1H" },
  { filterDay: "1", filterText: "1D" },
  { filterDay: "7", filterText: "1W" },
  { filterDay: "30", filterText: "1M" },
  { filterDay: "365", filterText: "1Y" },
];
export default function CoinDetail() {

  const [coin, setCoin] = useState(null);
  const [coinMarketData, setCoinMarketData] = useState(null);
  const [usdValue, setUsdValue] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [selectedRange, setSelectedRange] = useState("1");
  const route = useRoute();
  const {
    params: { coinId },
  } = route;
  const fetchCoinData = async () => {
    setIsLoading(true);
    const fetchedCoinData = await getDetailedCoinData(coinId);
    setCoin(fetchedCoinData);
    setUsdValue(fetchedCoinData.market_data.current_price.usd.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 }));
    setIsLoading(false);
  };

  const fetchMarketCoinData = async (selectedRangeValue) => {
    const fetchedCoinMarketData = await getCoinMarketChart(
      coinId,
      selectedRangeValue
    );
    setCoinMarketData(fetchedCoinMarketData);
  };

  useEffect(() => {
    fetchCoinData();
    fetchMarketCoinData(1);
  }, []);

  const normalizeMarketCap = (marketCap) => {
    if (marketCap > 1e12) {
      return `${(marketCap / 1e12).toFixed(1)}T`;
    }
    if (marketCap > 1e9) {
      return `${(marketCap / 1e9).toFixed(1)}B`;
    }
    if (marketCap > 1e6) {
      return `${(marketCap / 1e6).toFixed(1)}M`;
    }
    if (marketCap > 1e3) {
      return `${(marketCap / 1e3).toFixed(1)}K`;
    }
    return marketCap;
  };

  const normalizeTotalSupply = (total_supply) => {
    if (total_supply > 1e12) {
      return `${(total_supply / 1e12).toFixed(1)}T`;
    }
    if (total_supply > 1e9) {
      return `${(total_supply / 1e9).toFixed(1)}B`;
    }
    if (total_supply > 1e6) {
      return `${(total_supply / 1e6).toFixed(1)}M`;
    }
    if (total_supply > 1e3) {
      return `${(total_supply / 1e3).toFixed(1)}K`;
    }
    return total_supply;
  };

  const normalizeCirculatingSupply = (circulating_supply) => {
    if (circulating_supply > 1e12) {
      return `${(circulating_supply / 1e12).toFixed(1)}T`;
    }
    if (circulating_supply > 1e9) {
      return `${(circulating_supply / 1e9).toFixed(1)}B`;
    }
    if (circulating_supply > 1e6) {
      return `${(circulating_supply / 1e6).toFixed(1)}M`;
    }
    if (circulating_supply > 1e3) {
      return `${(circulating_supply / 1e3).toFixed(1)}K`;
    }
    return circulating_supply;
  };

  const [modalVisible, setModalVisible] = useState(false);
  const [priceVisible, setPriceVisible] = useState(false);

  const handlerSelectedRange = (selectedRangeValue) => {
    setSelectedRange(selectedRangeValue);
    fetchMarketCoinData(selectedRangeValue);
  };
  

  //fix rerender
  const memoOnSelectedRangeChange = React.useCallback(
    (range) => handlerSelectedRange(range),
    []
  );


  const screenWidth = Dimensions.get("window").width;
  const screenHeight = Dimensions.get("window").height;

  if (isLoading || !coin || !coinMarketData) {
    return <ActivityIndicator size="large" />;
  }
  const {
    id,
    image: { small },
    symbol,
    name,
    market_data: {
      atl,
      ath,
      high_24h,
      low_24h,
      market_cap,
      total_volume,
      total_supply,
      current_price,
      circulating_supply,
      price_change_percentage_24h,
    },
  } = coin;
  const { prices } = coinMarketData;




  const coinBalance = 2348;
  const fiatBalance = coinBalance * current_price.usd;
  const percentageColor = price_change_percentage_24h < 0 ? "#BC1B72" : "#008F87";
  const chartChangedColor =
    current_price.usd > prices[0][1] ? "#008F87" : "#BC1B72";



  function invokeHaptic() {
    haptics.impactAsync(haptics.ImpactFeedbackStyle.Light);
  }

  return (
    <View style={styles.container}>
      {/* <ChartPathProvider
        data={{
          // points: prices.map((price) => ({ x: price[0], y: price[1] })),
          points: prices.map(([x, y]) => ({ x, y })),
          // smoothingStrategy: "bezier",
        }}
      > */}
            <CoinDetailHeader
              coinId={id}
              image={small}
              symbol={symbol}
              name={name}
            />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{ flex: 1, paddingBottom: 180 }}>
          <LineChartProvider
            data={prices.map(([timestamp, value]) => ({ timestamp, value }))}
          >
            {/* CHART FILTER START */}
            <View style={{ paddingHorizontal: 15 }}>
              <View
                style={{
                  paddingBottom: 4,
                  flexDirection: "row",
                }}
              >
                <Text style={styles.priceTitle}>Current Price</Text>

              </View>
              {
                priceVisible ? (<View><View style={{ flexDirection: 'row' }}><Text style={styles.priceText}>$</Text><LineChart.PriceText style={styles.priceText} /></View></View>) : (<Text style={styles.priceText}>${current_price.usd.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 })}</Text>)
              }

              {
                priceVisible ? (<LineChart.DatetimeText style={styles.dateText} />) : (<Text style={[styles.percentage, { color: percentageColor }]}>
                  {price_change_percentage_24h < 0 ? "↓" : "↑"}{price_change_percentage_24h.toFixed(2).replace(/^-/, "")}% Today
                </Text>)
              }
            </View>
            <LineChart shape={shape.curveLinear} height={screenWidth / 2.5} width={screenWidth} style={{ alignSelf: 'center', marginTop: 20 }}>
              <LineChart.Path pathProps={{ strokeLinecap: 'round', strokeLinejoin: 'round' }} color={"rgba(0,140,182,1)"} width={1.5}>
                <LineChart.Gradient color={"rgba(0,140,182,1)"} />
              </LineChart.Path>
              <LineChart.CursorLine color="rgba(48,50,75,0.5)" />
              <LineChart.CursorCrosshair size={10} outerSize={0} onCurrentXChange={invokeHaptic()} color={"rgba(0,140,182,1)"} onActivated={() => setPriceVisible(true)} onEnded={() => setPriceVisible(false)} />
            </LineChart>
            <View style={styles.filterContainer}>
              {filterDaysArray.map((day) => (
                <Filter
                  key={day.filterDay}
                  filterDay={day.filterDay}
                  filterText={day.filterText}
                  selectedRange={selectedRange}
                  setSelectedRange={memoOnSelectedRangeChange}
                // setSelectedRange={handlerSelectedRange}
                />
              ))}
            </View>
            {/* CHART FILTER END */}
            {/* CHART COMPONENT START */}
            {/* CHART COMPONENT END */}
            {/* PRICE CONVERTER CONTAINER START */}
            <View style={[styles.row,{ borderBottomWidth:2, borderBottomColor:'rgba(230,230,233,0.5)', width:screenWidth, paddingHorizontal:15 }]}>
              <View style={[styles.priceConverterContainer, { width: screenWidth / 2 - 15, borderBottomWidth:0 }]}>
                <Text style={styles.textTitle}>Your balance</Text>
                <Text style={styles.text}>{coinBalance.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 8 })} {symbol.toUpperCase()}</Text>
              </View>
              <View style={[styles.priceConverterContainer, { width: screenWidth / 2 - 15, borderBottomWidth:0 }]}>
                <Text style={styles.textTitle}>Value</Text>
                <Text style={styles.text}>${fiatBalance.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 })} USD</Text>
              </View>
            </View>
            <View style={styles.row}>
              <View style={[styles.priceConverterContainer, { width: screenWidth / 2 - 15 }]}>
                <Text style={styles.textTitle}>Market Cap</Text>
                <Text style={styles.text}>${normalizeMarketCap(market_cap.usd)}</Text>
              </View>
              <View style={[styles.priceConverterContainer, { width: screenWidth / 2 - 15 }]}>
                <Text style={styles.textTitle}>24H Volume</Text>
                <Text style={styles.text}>${normalizeMarketCap(total_volume.usd)}</Text>
              </View>
            </View>
            <View style={styles.row}>
              <View style={[styles.priceConverterContainer, { width: screenWidth / 2 - 15 }]}>
                <Text style={styles.textTitle}>24H High</Text>
                <Text style={styles.text}>${high_24h.usd.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 })}</Text>
              </View>
              <View style={[styles.priceConverterContainer, { width: screenWidth / 2 - 15 }]}>
                <Text style={styles.textTitle}>24H Low</Text>
                <Text style={styles.text}>${low_24h.usd.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 })}</Text>
              </View>
            </View>
            <View style={styles.row}>
              <View style={[styles.priceConverterContainer, { width: screenWidth / 2 - 15 }]}>
                <Text style={styles.textTitle}>All-time High</Text>
                <Text style={styles.text}>${ath.usd.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 })}</Text>
              </View>
              <View style={[styles.priceConverterContainer, { width: screenWidth / 2 - 15 }]}>
                <Text style={styles.textTitle}>All-time Low</Text>
                <Text style={styles.text}>${atl.usd.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 })}</Text>
              </View>
            </View>
            <View style={styles.row}>
              <View style={[styles.priceConverterContainer, { width: screenWidth / 2 - 15 }]}>
                <Text style={styles.textTitle}>Total Supply</Text>
                <Text style={styles.text}>{normalizeTotalSupply(total_supply)}</Text>
              </View>
              <View style={[styles.priceConverterContainer, { width: screenWidth / 2 - 15 }]}>
                <Text style={styles.textTitle}>Circulating</Text>
                <Text style={styles.text}>{normalizeCirculatingSupply(circulating_supply)}</Text>
              </View>
            </View>
            {/* PRICE CONVERTER CONTAINER END */}
          </LineChartProvider>
        </View>
        {/* </ChartPathProvider> */}
      </ScrollView>
      <View style={{ alignSelf: 'center', backgroundColor: '#FFFF', position: 'absolute', paddingBottom: 10, bottom: 0 }}>

        <Modal
          style={{ justifyContent: "flex-end", top: 40, shadowOffset: { width: 0, height: 0 } }}
          animationType="slide"
          visible={modalVisible}
          backdropOpacity={1}
          hasBackdrop
          transparent={true}
          backdropColor="rgba(0,0,0,0.95)"
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
            setModalVisible(!modalVisible);
          }}>
          <View style={{ flex: 1, backgroundColor: '#FFFF', width: screenWidth, alignSelf: 'center', shadowOpacity: 0.5, shadowRadius: 100, borderRadius: 20 }}>
            <View style={styles.modalHeader}>
              <Pressable
                style={{ top: 2.5, left: 10, width: 40, height: 40, padding: 10, position: 'absolute' }}
                onPress={() => setModalVisible(!modalVisible)}>
                <Octicons
                  name="chevron-down"
                  size={26}
                  color="rgba(52,55,65,1)"
                />
              </Pressable>
            </View>
            <View style={{ height: screenHeight / 1.18 }}>
              <LinearGradient start={{ x: 1, y: 1 }} end={{ x: 1, y: 0 }} colors={['rgba(255,255,255,0)', 'rgba(255,255,255,0.95)']} style={{ height: 10, width: screenWidth, position: 'absolute', alignSelf: 'flex-start', zIndex: 9999 }} />
              <WebView scalesPageToFit={false} automaticallyAdjustContentInsets={true} originWhitelist={['*']} thirdPartyCookiesEnabled={true} sharedCookiesEnabled={true} source={{ uri: 'https://buy.onramper.com/?apiKey=pk_prod_01GSSZKP49X55Z0A3G0TVHK3A9&defaultCrypto=' + symbol }} />
            </View>
            <LinearGradient start={{ x: 1, y: 1 }} end={{ x: 1, y: 0 }} colors={['rgba(255,255,255,0.95)', 'rgba(255,255,255,0)']} style={{ height: 10, width: screenWidth, position: 'absolute', alignSelf: 'flex-end', zIndex: 9999, bottom: screenHeight / 22.5 }} />
          </View>
        </Modal>
        <LinearGradient start={{ x: 1, y: 1 }} end={{ x: 1, y: 0 }} colors={['rgba(255,255,255,0.95)', 'rgba(255,255,255,0)']} style={{ height: 40, width: screenWidth, position: 'absolute', alignSelf: 'flex-end', zIndex: 9999, bottom: screenHeight / 8.15 }} />
        <View style={{backgroundColor: '#FFFF', width: screenWidth }}>
          <AwesomeButton
            progress={modalVisible}
            width={340}
            height={50}
            onPress={() => setModalVisible(!modalVisible)}
            style={{ alignSelf: 'center', marginVertical: 20, bottom:6 }}
            progressLoadingTime={5000}
            backgroundColor={"rgba(0,140,182,1)"}
            borderRadius={8}
            raiseLevel={0}
            type="primary">
              <Text style={styles.buttonText}>Trade</Text>
          </AwesomeButton>
        </View>
      </View>
    </View>
  );
}
