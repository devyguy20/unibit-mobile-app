import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Home from "./../screens/Home";
import Pay from "./../screens/Pay"
import { Text } from "react-native";
import Favourites from "./../screens/FavouritesScreen";
import Svg, { Path, Circle } from 'react-native-svg';
import PortfolioScreen from "../screens/Portfolio";

const Tab = createBottomTabNavigator();
export default function BottomTabNavigator() {
  return (
    <Tab.Navigator
      initialRouteName="Market"
      screenOptions={{
        headerShown: false,
        tabBarActiveTintColor: "rgba(0,140,182,1)",
        tabBarInactiveTintColor: "#7C7E89",
        tabBarStyle: {
          height: 90,
          bottom: 4,
          borderTopWidth: 1,
          borderTopColor: "rgba(240,240,245,0.75)",
          backgroundColor: "#FFFF",
        },
      }}
    >
      <Tab.Screen
        name="Market"
        component={Home}
        options={{
          //color viene de las screenOptions
          tabBarLabel: ({ focused, color, size }) => (
            <Text style={{
              color: focused ? color : "rgba(84,86,90,0.9)",
              fontFamily: "LexendMedium",
              fontSize: 10.5,
              bottom: 6,
              letterSpacing: 0.15,
            }}>Market</Text>
          ),
          tabBarIcon: ({ focused, color }) => (
            <Svg
              width={focused ? 30 : 29.5}
              height={focused ? 30 : 29.5}
              viewBox="0 0 50 50"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <Path
                d="M6 40.5h37.76M34.5 10.439L43.612 9l-1.438 9.112"
                stroke={color}
                strokeWidth={focused ? 3.5 : 3.15}
                strokeLinecap="square"
                strokeLinejoin="round"
              />
              <Path
                d="M41 11.5L25.308 27.25l-6.539-6.562L7 32.5"
                stroke={color}
                strokeWidth={focused ? 3.5 : 3.15}
                strokeLinecap="square"
                strokeLinejoin="round"
              />
            </Svg>
          ),
        }}
      />
      <Tab.Screen
        name="Portfolio"
        component={PortfolioScreen}
        options={{
          tabBarLabel: ({ focused, color, size }) => (
            <Text style={{
              color: focused ? color : "rgba(84,86,90,0.9)",
              fontFamily: "LexendMedium",
              fontSize: 10.5,
              bottom: 6,
              letterSpacing: 0.15
            }}>Portfolio</Text>
          ),
          tabBarIcon: ({ focused, color }) => (
            <Svg
              width={focused ? 30 : 29.5}
              height={focused ? 30 : 29.5}
              viewBox="0 0 50 50"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <Path
                d="M24.25 9.1A17.152 17.152 0 1039 17.498L24.25 26.25V9.1z"
                stroke={color}
                strokeWidth={focused ? 3.5 : 3.15}
                strokeLinejoin="round"
              />
              <Path
                d="M24.25 4.75a21.5 21.5 0 0118.495 10.536L24.25 26.25V4.75zM24.25 26.25l14.5 8.595"
                stroke={color}
                strokeWidth={focused ? 3.5 : 3.15}
                strokeLinejoin="round"
              />
            </Svg>
          ),
        }}
      />
      <Tab.Screen
        name="Pay"
        component={Pay}
        options={{
          //color viene de las screenOptions
          tabBarLabel: ({ focused, color, size }) => (
            <Text style={{
              color: focused ? color : "rgba(84,86,90,0.9)",
              fontFamily: "LexendMedium",
              fontSize: 10.5,
              bottom: 6,
              letterSpacing: 0.15
            }}>Funds</Text>
          ),
          tabBarIcon: ({ focused, color }) => (
            <Svg
              width={focused ? 30 : 29.5}
              height={focused ? 30 : 29.5}
              viewBox="0 0 50 50"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <Path
                d="M42.291 17.36H9.041M8 33.492h33.25M15.46 22.852L8 17.426 15.46 12m19.37 16l7.462 5.426-7.461 5.426"
                stroke={color}
                strokeWidth={focused ? 3.5 : 3.15}
                strokeLinecap="square"
                strokeLinejoin="round"
              />
            </Svg>
          ),
        }}
      />

    </Tab.Navigator>
  );
}
