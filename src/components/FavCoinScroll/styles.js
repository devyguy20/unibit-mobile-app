import { StyleSheet } from "react-native";
const styles = StyleSheet.create({
  cardContainer: {
    height: 70,
    marginHorizontal:15,
    paddingVertical: 15,
  },
  title: {
    color: "rgba(52,55,65,1)",
    fontSize: 15,
    letterSpacing: 0.25,
    fontFamily: 'LexendMedium',
    fontWeight: "500",
  },

  ticket: {
    fontSize: 12.5,
    color: "rgba(48,50,75,0.65)",
    marginRight: 5,
    fontFamily: 'LexendRegular',
    textTransform: "uppercase",
  },
  itemColumn: {
  },
  price: {
    top: 10,
    fontSize: 15,
    fontFamily: 'RobotoMedium',
    alignSelf: 'flex-end',
    textAlign: 'right',
    fontWeight: "600",
    color: "rgba(52,55,65,1)",
  },
});
export default styles;
