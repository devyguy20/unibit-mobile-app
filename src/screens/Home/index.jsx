import { LinearGradient } from "expo-linear-gradient";
import React, { useEffect, useState } from "react";
import { FlatList, View, Text, RefreshControl, ScrollView, Dimensions, Pressable } from "react-native";
import { Ionicons, Octicons } from "@expo/vector-icons";
import Filter from "../../components/Filter";
import Favourites from "../FavouritesScreen";
import PageHeader from "../../components/PageHeader";
import CoinCard from "./../../components/CoinCard";
import CoinScroll from "./../../components/CoinScroll";
import { getMarketData } from "./../../services/requests";
import styles from "./styles";


const filterArray = [
  { filterValue: "All", filterText: "All assets" },
  { filterValue: "Tradable", filterText: "Tradable" },
  { filterValue: "Gainers", filterText: "Gainers" },
  { filterValue: "Losers", filterText: "Losers" },
];

const categories = [
  {
    title: 'Price',
  },
  {
    title: '24H Change',
  },
  {
    title: '24H High',
  },
  {
    title: '24H Low',
  },
  {
    title: 'Market Cap',
  },
];

export default function HomeScreen() {
  const [coins, setCoins] = useState([]);
  const [selectedRange, setSelectedRange] = useState("All");
  const percentageColor = 1 < 0 ? "#BC1B72" : "#008F87";
  const screenWidth = Dimensions.get("window").Width;
  const screenHeight = Dimensions.get("window").height;
  const [isLoading, setIsLoading] = useState(false);

  const handlerSelectedRange = (selectedRangeValue) => {
    setSelectedRange(selectedRangeValue);
    fetchMarketCoinData(selectedRangeValue);
  };
  

  //fix rerender
  const memoOnSelectedRangeChange = React.useCallback(
    (range) => handlerSelectedRange(range),
    []
  );
  const fetchMarketCoinData = async (selectedRangeValue) => {
    const fetchedCoinMarketData = await getCoinMarketChart(
      coinId,
      selectedRangeValue
    );
    setCoinMarketData(fetchedCoinMarketData);
  };

  const fetchCoins = async (page) => {
    if (isLoading) {
      return;
    }
    setIsLoading(true);
    const coinsData = await getMarketData(page);
    setCoins((prevCoins) => [...prevCoins, ...coinsData]);
    setIsLoading(false);
  };
  const refetchCoins = async () => {
    if (isLoading) {
      return;
    }
    setIsLoading(true);
    const coinsData = await getMarketData();
    setCoins(coinsData);
    setIsLoading(false);
    console.log('fetchi')
  };
  useEffect(() => {
    console.log("fetchin")
    fetchCoins();
  }, []);



//    <Text style={[styles.percentage, { color: percentageColor }]}>
//    {price_change_percentage_24h < 0 ? "↓" : "↑"}{price_change_percentage_24h.toFixed(2).replace(/^-/, "")}% Today
//    </Text>



  return (
    <View style={{ backgroundColor: '#FFFF' }} showsVerticalScrollIndicator={false}
      refreshControl={
        <RefreshControl
          refreshing={isLoading}
          tintColor="darkgrey"
          onRefresh={refetchCoins}
        />
      }>
      <LinearGradient start={{ x: -0.1, y: 0 }} end={{ x: 0.75, y: 0 }} colors={['rgba(255,255,255,0)', 'rgba(255,255,255,0.95)']} style={{ height: screenHeight * 15, width: 50, position: 'absolute', alignSelf: 'flex-end', zIndex: 9999 }} />
      <PageHeader title={"Market"}/>
      <View style={styles.filterContainer} zIndex={999999}>
              {filterArray.map((value) => (
                <Filter
                  key={value.filterValue}
                  filterValue={value.filterValue}
                  filterText={value.filterText}
                  selectedRange={selectedRange}
                  setSelectedRange={memoOnSelectedRangeChange}
                // setSelectedRange={handlerSelectedRange}
                />
              ))}
            </View>
      <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ flexDirection: "row", paddingBottom: 32 }}>
        <LinearGradient start={{ x: 0.25, y: 0 }} end={{ x: 1, y: 0 }} colors={['rgba(255,255,255,0.95)', 'rgba(255,255,255,0)']} style={{ left: 115, height: screenHeight * 15, width: 15, position: 'absolute', alignSelf: 'flex-start', zIndex: 9999 }} />
        <FlatList
          bounces={false}
          paddingBottom={150}
          style={{ width: 170 }}
          ListHeaderComponent={({ item }) => {
            return (
              <FlatList
                horizontal
                scrollEnabled={false}
                showsHorizontalScrollIndicator={false}
                ListHeaderComponent={({ item }) => {
                  return (<View style={{ height: 38, width: 60, alignSelf: 'flex-start' }}><Text style={styles.textTitle}>Asset</Text></View>)
                }}
                renderItem={({ item }) => <View style={{ height: 38, alignSelf: 'flex-end', width: 105 }} />}
                keyExtractor={item => item.title}

              />)
          }}
          data={coins.slice(0, 150)}
          renderItem={({ item }) => <CoinCard marketCoin={item} />}
          keyExtractor={(contact, index) => String(index)}
        />
        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          <FlatList
            ListHeaderComponent={({ item }) => {
              return (<FlatList
                horizontal
                data={categories}
                scrollEnabled={false}
                showsHorizontalScrollIndicator={false}
                renderItem={({ item }) => <View style={{ height: 38, alignSelf: 'flex-end', width: 105 }}><Text style={styles.textTitle}>{item.title}</Text></View>}
                keyExtractor={item => item.title}

              />)
            }}
            bounces={false}
            data={coins.slice(0, 150)}
            renderItem={({ item }) => <CoinScroll marketCoin={item} />}
            showsHorizontalScrollIndicator={false}
            keyExtractor={(contact, index) => String(index)}
          />
        </ScrollView>
      </ScrollView>
    </View>
  );
}
